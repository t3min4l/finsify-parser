import os 
import sys 
import re
import json

root = os.path.abspath(os.path.dirname(__file__))
log_path = os.path.join(root, 'log.txt')

log_file = open(log_path, 'r')
lines = log_file.readlines()

slicing_index = 0

for i, text in enumerate(lines):
	if text == '*** main.py ***\n':
		slicing_index = i 
print(slicing_index)
flows = lines[85:len(lines)]
map(str.lstrip, flows)
# map(str.replace("\n",""), flows)
flow_key_word = '->'
flow_key_word_2 = '-->'
new_flows = list()
for string in flows:
	temp = string.replace("\n", "")
	new_string = temp.replace("\t", "")
	if flow_key_word in new_string and flow_key_word_2 not in new_string:
		string_to_append = new_string.replace(" ", "")
		new_flows.append(string_to_append)


nodes = list()
paths = list()
idx = 0
print(new_flows)
for string in new_flows:
	node_list = string.split(flow_key_word)
	node1 = dict()
	node2 = dict()
	path = dict()

	if not any(d['label'] == node_list[0] for d in nodes):
		if idx == 0:
			node1['id'] = idx 
			node1['label'] = node_list[0]
			idx += 1
		else:
			node1['id'] = idx 
			node1['label'] = node_list[0]
			idx += 1
		nodes.append(node1)
	else:
		for d in nodes:
			if d['label'] == node_list[0]:
				node1['id'] = d['id']
				node1['label'] = d['label']
	if not any(d['label'] == node_list[1] for d in nodes):
		if idx == 0:
			node2['id'] = idx 
			node2['label'] = node_list[1]
			idx += 1
		else:
			node2['id'] = idx 
			node2['label'] = node_list[1]
			idx += 1
		nodes.append(node2)
	else:
		for d in nodes:
			if d['label'] == node_list[1]:
				node2['id'] = idx 
				node2['label'] = d['label']
	# print(node1)
	# print(node2) 

	path['source'] = node1['id']
	path['target'] = node2['id']

	paths.append(path)

print(nodes)
print(paths)

data = dict()
data['nodes'] = nodes 
data['paths'] = paths

with open('sample.json', 'w') as outputfile:
	json.dump(data, outputfile)