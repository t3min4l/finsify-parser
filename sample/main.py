from class1 import Class1 
from class2 import Class2 
import sys 
import os 


c1 = Class1()
c2 = Class2()

def printFooBar(c1):
	c1.foo()
	c1.bar()

def printNameJob(c2):
	c2.printName("Nam")
	c2.printJob("Culi")

printFooBar(c1)
printNameJob(c2)

sys.stdout = open('log.txt', 'w')
